#!/usr/bin/env node

import { promises as fs } from 'fs'
import { fileURLToPath } from 'url'
import path from 'path'
import { parseArgs } from 'node:util'


const JSON_STYLES_FILE_NAME = 'styles.json'
const JSON_RADIOS_FILE_NAME = 'radios.json'
const JSON_RADCAP_FILE_NAME = 'radcap.json'


const doParseArgs = () => {
  const options = {
    help: {
      short: 'h',
      type: 'boolean',
    },
  }
  try {
    return parseArgs({
      options,
    })
  } catch (error) {
    exitWithError('syntax error!')
  }
}

const processArgs = async (args) => {
  let { values: options, positionals: _ } = args
  if (!Object.keys(options).length) {
    await checkRequiredFileExist(JSON_STYLES_FILE_NAME)
    await checkRequiredFileExist(JSON_RADIOS_FILE_NAME)
    const styles = await loadJSONfile(JSON_STYLES_FILE_NAME)
    const radios = await loadJSONfile(JSON_RADIOS_FILE_NAME)
    generateRadcapJson(styles, radios)
  }
  // has some args
  if (options.help) {
    showSyntax()
    return
  }
}

const checkRequiredFileExist = async (fileName) => {
  const fileExists = await isFilePresent(fileName)
  if (!fileExists) {
    exitWithError(`required file missing: ${fileName}`)
  }
}


const exitWithError = (error) => {
  console.log(`ERROR: ${error}`)
  showSyntax()
  process.exit(1)
}

const showSyntax = () => {
  const scriptName = path.basename(new URL(import.meta.url).pathname)
  const help = [
    'mk-radcap, a radcap.json generator for radcap.js',
    'usage:',
    `  ${scriptName} [options]`,
    'options:',
    '  -h, --help: show this help screen',
    'requires: nodejs, files: radios.json and styles.json from radcap apk',
  ]
  for (let line of help) {
    console.log(line)
  }
}

const isFilePresent = async (fileName) => {
  return !!(await fs.stat(getFullPath(fileName))
    .catch((_) => false))
}

const loadJSONfile = async (fileName) => {
  const file = await fs.readFile(getFullPath(fileName), 'utf8')
  return await JSON.parse(file)
}

const getFullPath = (fileName) => {
  const dirName = path.dirname(fileURLToPath(import.meta.url))
  return path.resolve(dirName, fileName)
}

const generateRadcapJson = (styles, radios) => {
  const radcap = {}
  for (const style of styles.styles) {
    radcap[style.style] = radios.radios
      .filter((radio) => radio.style == style.id)
      .map((radio) => ({
        name: radio.name,
        url: radio.url,
      }))
  }
  fs.writeFile(getFullPath(JSON_RADCAP_FILE_NAME), JSON.stringify(radcap, null, 2));
}


// 'main'
const args = doParseArgs()
await processArgs(args)
