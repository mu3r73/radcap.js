#!/usr/bin/env node
import { promises as fs } from 'fs'
import { fileURLToPath } from 'url'
import path from 'path'
import axios from 'axios'
import { parseArgs } from 'node:util'
import { spawn } from 'child_process'
import inquirer from 'inquirer'
import inquirerPrompt from 'inquirer-autocomplete-prompt'
inquirer.registerPrompt('autocomplete', inquirerPrompt)


const JSON_FILE_NAME = 'radcap.json'

const JSON_URL = 'https://gist.githubusercontent.com/leejunip/741d5a8330262b73b53eec676e44ec70/raw/def6eb19d7bfe461b075b71c46cc5461fac72449/radcap.json'


const doParseArgs = () => {
  const options = {
    help: {
      short: 'h',
      type: 'boolean',
    },
    menu: {
      short: 'm',
      type: 'boolean',
    },
    rnd: {
      short: 'r',
      type: 'boolean',
    },
    genre: {
      short: 'g',
      type: 'string',
    },
    subgenre: {
      short: 's',
      type: 'string',
    },
  }
  try {
    return parseArgs({
      options,
      allowPositionals: true,
    })
  } catch (error) {
//    console.log('unknown option')
  }
}

 
const processArgs = async (args) => {
  if (!args) {
    warnSyntaxError()
    return
  }
  // has some args
  let { values: options, positionals: filter } = args
  if (options.help) {
    showSyntax()
    return 
  }
  // has some valid, non-help args
  filter = filter.join(' ')
  // load json file
  const fileExists = await isJSONfilePresent() 
  if (!fileExists) {
    await downloadJSONfile()
  }
  const jsonData = await loadJSONfile()
  // decide what to do
  if (hasNoOptionsAndNoFilters(options, filter)) {
    await showUnfilteredMenu(jsonData)
    return
  }
  if (options.menu) {
    // any other args are discarded
    await showGenresMenu(jsonData)
    return
  }
  if (!!options.genre) {
    await playByGenreNum(jsonData, options, filter)
    return
  } 
  if (options.rnd) {
    // genre is discarded (the case was handled above, as genre with options.random)
    playRndStationMatching(jsonData, filter)
    return
  }
  await showFilteredMenu(jsonData, filter)
}


const hasNoOptionsAndNoFilters = (options, filter) => {
  return (!(filter || '').length && !Object.keys(options).length)
}


const warnSyntaxError = () => {
  console.log('syntax error!')
  showSyntax()
}


const showSyntax = () => {
  const scriptName = path.basename(new URL(import.meta.url).pathname)
  const help = [
    'radcap, a selector for radio caprice stations',
    'usage:',
    `  ${scriptName} [<filter_string>] [options] [suboptions]`,
    'options:',
    '  -h, --help: show this help screen',
    '  -m, --menu: show genre/subgenre menu',
    '  -r, --rnd: play random station',
    '  -g, --genre [NUMBER]: choose a genre by number',
    'suboptions:',
    '  -s, --subgenre [NUMBER]: choose a subgenre by number (requires: --genre)',
    'examples:',
    `  ${scriptName} --menu                asks which genre / subgenre to play`,
    `  ${scriptName} --rnd                 plays a random station`,
    `  ${scriptName} jazz                  asks which *jazz* station to play`,
    `  ${scriptName} 'sludge metal'        plays the "sludge metal" station`,
    `  ${scriptName} 'death metal' --rnd   plays a random *death metal* station`,
    `  ${scriptName} -g 7 -s 46            plays genre 7, subgenre 46`,
    `  ${scriptName} --genre 1             asks which genre 1 subgenre to play`,
    `  ${scriptName} -g 6 -r               plays a random genre 6 station`,
    'requires: mpv, nodejs',
  ]
  for (let line of help) {
    console.log(line)
  }
}


const isJSONfilePresent = async () => {
  return !!(await fs.stat(getFullPath())
    .catch((_) => false))
}


const downloadJSONfile = async () => {
  const response = await axios.get(JSON_URL, {
    transformResponse: (response) => response
  })
  const json = response.data
  return await fs.writeFile(JSON_FILE_NAME, json)
}


const loadJSONfile = async () => {
  const file = await fs.readFile(getFullPath(), 'utf8')
  return await JSON.parse(file)
}


const getFullPath = () => {
  const dirName = path.dirname(fileURLToPath(import.meta.url))
  return path.resolve(dirName, JSON_FILE_NAME)
}


const playByGenreNum = async (jsonData, options, filter) => {
  const genreNum = options.genre
  const subgenreNum = options.subgenre
  checkValidGenreSubgenreNums(genreNum, subgenreNum)
  // genre/subgenre are valid
  if (!subgenreNum && !options.rnd && !(filter || '').length) {
    await showGenreNumMenu(jsonData, genreNum)
    return
  }
  // we have some option or filter
  if (!!subgenreNum) {
    // if present, filter is discarded
    playRadioStation(jsonData, genreNum, subgenreNum)
    return
  }
  if (options.rnd) {
    playRndGenreNumStation(jsonData, genreNum, filter)
    return
  }
  // we only have a filter
  await showFilteredGenreMenu(jsonData, genreNum, filter)
}


const showUnfilteredMenu = async (jsonData) => {
  let subgenres = Object.values(jsonData).flat()
  const subgenresNames = subgenres.map(sg => sg.name).sort()
  subgenresNames.unshift('(random)')
  const subgenreName = await pickOptionFromMenu('Subgenre', subgenresNames)
  playSubgenre(subgenreName, subgenresNames, subgenres)
}


const showGenresMenu = async (jsonData) => {
  const genres = getSortedGenres(jsonData)
  genres.unshift('(random)')
  const genreName = await pickOptionFromMenu('Genre', genres)
  console.log('DEBUG', genreName)
  checkValidGenreName(genreName, genres)
  // we have a valid genre name
  console.log()
  console.log(`chosen genre: ${genreName}`)
  await showSubgenresMenu(jsonData, genreName)
}


const showSubgenresMenu = async (jsonData, genreName) => {
  if (genreName === '(random)') {
    const genres = Object.keys(jsonData)
    genreName = genres[Math.floor(Math.random() * genres.length)]
    console.log(`random genre: ${genreName}`)
  }
  const subgenres = jsonData[genreName]
  const subgenresNames = getSortedSubgenresNames(subgenres)
  subgenresNames.unshift('(random)')
  console.log()
  const subgenreName = await pickOptionFromMenu(`${genreName} Subgenre`, subgenresNames)
  playSubgenre(subgenreName, subgenresNames, subgenres)
}


const showFilteredMenu = async (jsonData, filter) => {
  let subgenres = Object.values(jsonData).flat()
  if (!!filter) {
    subgenres = subgenres.filter(sg =>
      sg.name.toLowerCase().includes(filter.toLowerCase()))
  }
  checkValidSubgenres(subgenres)
  // subgenres is valid
  const subgenresNames = subgenres.map(sg => sg.name).sort()
  if (subgenres.length === 1) {
    playSubgenre(subgenresNames[0], subgenresNames, subgenres)
  } else {
    subgenresNames.unshift('(random)')
    const subgenreName = await pickOptionFromMenu('Subgenre', subgenresNames)
    playSubgenre(subgenreName, subgenresNames, subgenres)
  }
}


const showGenreNumMenu = async (jsonData, genreNum) => {
  const genreName = getGenreName(jsonData, genreNum)
  await showSubgenresMenu(jsonData, genreName)
}


const showFilteredGenreMenu = async (jsonData, genreNum, filter) => {
  const genreName = getGenreName(jsonData, genreNum)
  let subgenres = getFilteredSubgenres(jsonData, genreName, filter)
  const subgenresNames = subgenres.map(sg => sg.name).sort()
  if (subgenres.length === 1) {
    playSubgenre(subgenresNames[0], subgenresNames, subgenres)
  } else {
    subgenresNames.unshift('(random)')
    const subgenreName = await pickOptionFromMenu(`${genreName} Subgenre`, subgenresNames)
    playSubgenre(subgenreName, subgenresNames, subgenres)
  }
}


const pickOptionFromMenu = async (title, options) => {
  const message = `${title}:`
  const res = await inquirer.prompt([
    {
      type: 'autocomplete',
      name: 'choice',
      message,
      choices: options,
      source: (_, input='') => {
        return new Promise(async (resolve) => {
          const search = options.filter((opt) => opt.toLowerCase().includes(input.toLowerCase()))
          resolve(search)
        })
      },
    },
  ])
  return res.choice
}


const playRndStationMatching = (jsonData, filter) => {
  let subgenres = getSortedFilteredSubgenreNames(jsonData, filter)
  checkValidSubgenres(subgenres)
  // subgenres is valid
  playRndSubgenreStation(subgenres)
}


const getSortedFilteredSubgenreNames = (jsonData, filter) => {
  let subgenres = Object.values(jsonData).flat()
  if (!!filter) {
    subgenres = subgenres.filter(sg =>
        sg.name.toLowerCase().includes(filter.toLowerCase()))
  }
  return subgenres
}


const playRndGenreNumStation = (jsonData, genreNum, filter) => {
  const genreName = getGenreName(jsonData, genreNum)
  playRndGenreStation(jsonData, genreName, filter)
}


const playRndGenreStation = (jsonData, genreName, filter) => {
  let subgenres = getFilteredSubgenres(jsonData, genreName, filter)
  playRndSubgenreStation(subgenres)
}


const playSubgenre = (subgenreName, subgenresNames, subgenres) => {
  checkValidSubgenreName(subgenreName, subgenresNames)
  // we have a 'valid' subgenre name
  console.log()
  console.log(`chosen subgenre: ${subgenreName}`)
  if (subgenreName === '(random)') {
    playRndSubgenreStation(subgenres)
    return
  }
  // chosen subgenre isn't (random)
  const subgenre = getSubgenre(subgenres, subgenreName)
  playURL(subgenre.url)
}


const playRndSubgenreStation = (subgenres) => {
  const subgenre = subgenres[Math.floor(Math.random() * subgenres.length)]
  console.log(`random subgenre: ${subgenre.name}`)
  playURL(subgenre.url) 
}


const playRadioStation = (jsonData, genreNum, subgenreNum) => {
  const genreName = getGenreName(jsonData, genreNum)
  console.log(`genre: ${genreName}`)
  const subgenres = getSubgenresSortedByName(jsonData, genreName, subgenreNum)
  checkValidSubgenreNum(subgenres, subgenreNum)
  // subgenreNum is valid
  const subgenre = subgenres[subgenreNum - 1]
  console.log(`subgenre: ${subgenre.name}`)
  playURL(subgenre.url)  
}


const getSubgenresSortedByName = (jsonData, genreName) => {
  const subgenres = jsonData[genreName]
  return subgenres.sort((sg1, sg2) => sg1.name.localeCompare(sg2.name))
}


const getSortedSubgenresNames = (subgenres) => {
  return subgenres.map(sg => sg.name)
      .sort((sgn1, sgn2) => sgn1.localeCompare(sgn2))
}


const getSortedGenres = (jsonData) => {
  return Object.keys(jsonData)
    .sort((g1, g2) => g1.localeCompare(g2)) 
}

 
const getFilteredSubgenres = (jsonData, genreName, filter) => {
  let subgenres = jsonData[genreName]
  if (!!filter) {
    subgenres = subgenres.filter(sg => 
      sg.name.toLowerCase().includes(filter.toLowerCase()))
  }
  checkValidSubgenres(subgenres)
  // subgenres is valid
  return subgenres
}


const getSubgenre = (subgenres, subgenreName) => {
  const subgenre = subgenres.find(sg =>
    sg.name === subgenreName)
  checkValidSubgenre(subgenre)
  // subgenre is valid
  return subgenre
}


const getGenreName = (jsonData, genreNum) => {
  const genres = getSortedGenres(jsonData)
  checkValidGenreNum(genres, genreNum)
  // genreNum is valid
  return genres[genreNum - 1]
}


const playURL = (url) => {
  console.log(`\nPlaying ${url}`)
  const mpv = spawn( 'mpv', [ url ], { stdio: 'inherit' } )
  mpv.on('exit', () => {
    process.exit()
  })
}


const checkValidGenreSubgenreNums = (genreNum, subgenreNum) => {
  if (isNaN(genreNum)
      || (!!subgenreNum && isNaN(subgenreNum))
  ) {
    warnSyntaxError()
    process.exit()
  }
}


const checkValidGenreName = (genreName, genres) => {
  if (!genreName || !genres.includes(genreName)) {
    console.log()
    console.log('no genres match the specified criteria')
    process.exit()
  }
}

const checkValidSubgenreName = (subgenreName, subgenresNames) => {
  if (!subgenreName || !subgenresNames.includes(subgenreName)) {
    console.log()
    console.log('no subgenres match the specified criteria')
    process.exit()
  }
}

const checkValidSubgenres = (subgenres) => {
  if (!subgenres.length) {
    console.log('no subgenres match the specified criteria')
    process.exit()
  }
}


const checkValidSubgenre = (subgenre) => {
  if (!subgenre) {
    console.log('no subgenres match the specified criteria')
    process.exit()
  }
}


const checkValidGenreNum = (genres, genreNum) => {
  checkItemInRange(genres, genreNum, 'genre')
}


const checkValidSubgenreNum = (subgenres, subgenreNum) => {
  checkItemInRange(subgenres, subgenreNum, 'subgenre')
}


const checkItemInRange = (itemsList, itemIndex, itemLabel) => {
  if ((itemIndex < 1) || (itemIndex > itemsList.length)) {
    console.log(`${itemLabel} should be in range 1 .. ${itemsList.length}`)
    process.exit()
  }
}

 
// 'main'
const args = doParseArgs()
await processArgs(args)
